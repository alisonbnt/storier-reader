<?php

$story = array(
    'title' => "A Ponte",
    'author' => "Alison Bento",
    'co-authors' => array(
        'Murilo Bento',
        'Ana Eliza Praxedes de Sa'
    ),
    'sinopse' => "Can you help the brave knight to cross the dark bridge?",
    'strong-contents' => array(
        'violence'
    ),
    'first_chapter' => 'ch1',
    
    'chapters' => array(
        'ch1' => array(
            'title' => 'Sign of Danger',
            'content' => 'Nulla pellentesque nunc a scelerisque pellentesque. Nam sagittis consectetur varius. Phasellus tempus tellus a tristique eleifend. Proin non ante vel nisl sollicitudin interdum. Nulla facilisi. Aenean eget mi eu ipsum vestibulum pulvinar. Pellentesque vel luctus nunc, nec ullamcorper lacus. Phasellus ac purus interdum massa pretium vehicula.',
            'atmosphere' => 'neutral',
            'choice_chapter' => array(
                'question' => 'What are you going to do?',
                'choices' => array(
                    array('description' => 'Follow the crowd', 'redirect' => 'ch2'),
                    array('description' => 'Stop where you are', 'redirect' => 'ch3')
                )
            )
        ),
        'ch2' => array(
            'title' => 'Another Chapter',
            'content' => ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non felis vitae libero aliquam congue. Nulla fringilla et purus eget pellentesque. Nulla nec nisl vel felis venenatis faucibus elementum ac nisi. Nullam molestie, est at imperdiet rutrum, elit tortor accumsan lectus, vel venenatis felis sem sit amet dui. Maecenas ac viverra elit, quis dapibus ante. Sed a elit luctus purus tincidunt porttitor eget sit amet libero. Nulla cursus convallis orci, sit amet vehicula lectus scelerisque nec. Quisque leo mi, faucibus eu vulputate et, placerat eget eros. Pellentesque eros libero, condimentum in velit ut, porta euismod tortor. Vivamus neque libero, varius faucibus placerat at, vulputate vitae turpis. Morbi eu purus ac dui cursus consequat. Mauris venenatis molestie ipsum quis ullamcorper. Nam eget laoreet eros.',            
            'atmosphere' => 'neutral',
            'ending_text' => array(
                'text' => 'Assim termina sua aventura',
                'color' => 'red'
            )
        )
    )
    
);

echo json_encode($story);
