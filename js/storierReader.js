var _story;
var _currentChapter;
var _path = [];

var _supportedColors = {
    "black": "storier-black",
    "white": "storier-white",
    "red": "storier-red",
    "dark_green": "storier-dark-green"
};

// Obtem a estória de uma url
function getStory(url) {
    var mygetrequest = new ajaxRequest();

    mygetrequest.onreadystatechange = function() {
        if (mygetrequest.readyState === 4) {
            if (mygetrequest.status === 200 || window.location.href.indexOf("http") === -1) {
                var jsondata = JSON.parse(mygetrequest.responseText); //retrieve result as an JavaScript object
                parseStory(jsondata);
            }
            else {
                alert("An error has occured making the request");
            }
        }
    };

    mygetrequest.open("GET", url, true);
    mygetrequest.send(null);
}


// A partir da estrutura da estória, escolhe o capítulo inicial
function parseStory(story) {
    if (isset(story.chapters)) {
        _story = story;
        _currentChapter = story.first_chapter;
    }
    selectChapter(story.first_chapter);
}

function selectChapter(chapterId) {
    if (isset(_story.chapters[chapterId])) {
		showStoryView();
        //$("#story-canvas").animate({ scrollTop: 0 }, 'slow');
        document.getElementById('story-canvas').scrollTop = 0;

        _path.push(chapterId);

        var localChapter = _story.chapters[chapterId];
        document.getElementById('reader-title').innerHTML = localChapter.title;
        document.getElementById('reader-text-content').innerHTML = localChapter.content;
        document.getElementById('loading-screen').style.display = 'none';
        document.getElementById('reader-choices').innerHTML = '';

        if (isset(localChapter.choice_chapter)) {
            // It's a choice chapter
            setChoices(localChapter);
        } else if(isset(localChapter.ending_text)){
            // It's an ending chapter
            setEnding(localChapter, chapterId);
        }
    } else {
        alert('invalid chapter');
    }
}

function setChoices(chapter) {
    document.getElementById('reader-choices').style.display = 'block';
    //document.getElementById('reader-choices').innerHTML = '';

    var lineElement = document.createElement('hr');
    var questionElement = document.createElement('p');
    
    questionElement.innerHTML = chapter.choice_chapter.question;

    document.getElementById('reader-choices').appendChild(lineElement);
    document.getElementById('reader-choices').appendChild(questionElement);

    var shuffled = shuffle(chapter.choice_chapter.choices);

    for (var count = 0; count < shuffled.length; count++) {
        var choice = shuffled[count];
        var choiceElement = document.createElement('a');
        choiceElement.setAttribute('onclick', 'selectChapter(this.dataset.chapter)');
        choiceElement.setAttribute('data-chapter', choice.redirect);
        choiceElement.setAttribute('href', 'javascript:void(0)');

        choiceElement.innerHTML = choice.description;

        var clearfix = document.createElement('div');
        clearfix.setAttribute('class', 'clearfix choice-separator');

        document.getElementById('reader-choices').appendChild(choiceElement);
        document.getElementById('reader-choices').appendChild(clearfix);
    }
}

function setEnding(endingChapter, endingId) {
    var endingText = endingChapter.ending_text.text;
    document.getElementById('reader-ending-text').innerHTML = endingText;
    
    if(isset(endingChapter.ending_text.color)){
        if(isset(_supportedColors[endingChapter.ending_text.color])){
            var previousClass = document.getElementById('reader-ending-text').getAttribute('class');
            var colorIndex = endingChapter.ending_text.color;
            document.getElementById('reader-ending-text').setAttribute('class', previousClass + ' ' + _supportedColors[colorIndex]);
        }
    }
    
    var actualPath;
    var travel;

    for (travel = 0; travel < (_path.length - 1); travel++) {
        // O Chapter atual
        actualPath = _path[travel];

        // O ponteiro para o chapter seguinte
        var nextChapterPointer = _path[travel + 1];

        // A escolha que o levou ao chapter seguinte
        var choice = findChoice(actualPath, nextChapterPointer);
        
        var pathElement = createPathDescription(actualPath, choice, endingId);
        
        document.getElementById('travel-path').appendChild(pathElement);
    }
    
    
    var finalChapter = _story.chapters[_path[travel]];
    var finalElement = document.createElement('p');
    finalElement.setAttribute('class', 'reader-ending-path text-center path-item');
    var innerHtml = 'Tudo terminou em <strong>' + finalChapter.title + '</strong>';
    finalElement.innerHTML = innerHtml;

    document.getElementById('travel-path').appendChild(finalElement);

    revealEnding();
}

function createPathDescription(chapterPointer, choiceStructure, endingId){
    var chapterStructure = _story.chapters[chapterPointer];
    var nextChapterStructure = _story.chapters[choiceStructure.redirect];
    
    var pElement = document.createElement('p');
    pElement.setAttribute('class', 'reader-ending-path text-center path-item');
    
    var innerHtml = 'Em <a href="javascript:void(0)" class="chapter-link" onclick="resetStory(\'' + chapterPointer + '\')">' + chapterStructure.title + '</a>, ' + decode_utf8('você') + ' escolheu "<span class="text-italic">' + choiceStructure.description + '</span>", indo para ';
    
    if(choiceStructure.redirect === endingId){
		innerHtml = innerHtml + '<strong>' + nextChapterStructure.title + '</strong>';
    }else{
		innerHtml = innerHtml + '<a href="javascript:void(0)" class="chapter-link" onclick="resetStory(\'' + choiceStructure.redirect + '\')">' + nextChapterStructure.title + '</a>';
	}
    
    pElement.innerHTML = innerHtml;
    
    return pElement;
}

function findChoice(actual, next) {
    var choice;
    var pChapter = _story.chapters[actual];

    for (var lchoice in pChapter.choice_chapter.choices) {
        if (pChapter.choice_chapter.choices[lchoice].redirect === next) {
            choice = pChapter.choice_chapter.choices[lchoice];
        }
    }

    return choice;
}

function revealEnding() {
    document.getElementById('ending-screen').style.display = "block";
    document.getElementById('reader-choices').style.display = "none";
}


function callLoadScreen() {
    document.getElementById('reader-title').innerHTML = '';
    document.getElementById('reader-text-content').innerHTML = '';
    document.getElementById('reader-choices').style.display = 'none';

    document.getElementById('loading-screen').style.display = 'block';
}

function showStoryView(){
	document.getElementById('ending-screen').style.display = "none";
	document.getElementById('travel-path').innerHTML = '';
    document.getElementById('reader-choices').style.display = "block";
}

function resetStory(chapter){
	if(isset(chapter)){
		_currentChapter = chapter;
	}else{
		_currentChapter = _story.first_chapter;
	}
	
	_path = [];
	selectChapter(_currentChapter);
}

function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}

function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

